package jo.edu.ju.signupfirebaseauthen

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseException
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var callbacks : PhoneAuthProvider.OnVerificationStateChangedCallbacks
    lateinit var mAuth : FirebaseAuth
    var verification = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mAuth = FirebaseAuth.getInstance()

        bSubmit.setOnClickListener {
            checkCode()
        }

        bSignUp.setOnClickListener {

            if (validation()) {
                verificationCallbacks()
                var phoneNu = etPhone.text.toString()
                PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNu, 60, TimeUnit.SECONDS, this, callbacks)
            }
        }
    }

    //-----------------------------FUNCTIONS---------------------

    fun verificationCallbacks() {
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {}

            override fun onVerificationFailed(e: FirebaseException) {}

            override fun onCodeSent(verificationId: String?, token: PhoneAuthProvider.ForceResendingToken) {
               super.onCodeSent(verificationId,token)
                verification = verificationId.toString()
            }
        }
    }

    //-------------------------------------------------------------------------

    fun validation(): Boolean {
        var fullName = etName.text.toString()
        var phoneNo = etPhone.text.toString()

        var valid: Boolean = true

        if (fullName.isEmpty()) {
            etName.error = "Please, enter your name"
            valid = false
        }

        if (phoneNo.isEmpty()) {
            etPhone.error = "Please, enter your phone"
            valid = false
        }

        return valid
    }

    //-------------------------------------------------------------------------

    fun signIn(credential: PhoneAuthCredential) {
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener{
                task : Task<AuthResult> ->
                if(task.isSuccessful) {
                    var intent : Intent = Intent(this@MainActivity, HomeActivity::class.java)
                    startActivity(intent)
                }

            }
    }

    //-------------------------------------------------------------------------

    fun checkCode(){
        val textCode = etCode.text.toString()
        val credential : PhoneAuthCredential = PhoneAuthProvider.getCredential(verification, textCode)
        signIn(credential)
    }
}
